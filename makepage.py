#!/usr/bin/env python3

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import time
import datetime
import math

matplotlib.use('Agg')

plt.style.use('_mpl-gallery')

f = open("index.html","w")

f.write("<!DOCTYPE html>\n")
#f.write("<html><head><title>umail.today</title></head>\n")
f.write("<html><head><title>umail.today</title><link rel=\"stylesheet\" type=\"text/css\" rel=\"noopener\" target=\"_blank\" href=\"mystyles.css\"></head>\n")

f.write("<body>\n")


tz = datetime.datetime.now(datetime.timezone.utc).astimezone().tzinfo;

def utc_to_local(utc_dt):
    return utc_dt.replace(tzinfo=timezone.utc).astimezone(tz=None)

xgfs_normal12 = [(235.0/255, 172.0/255, 35.0/255), (184.0/255, 0.0/255, 88.0/255), (0.0/255, 140.0/255, 249.0/255), (0.0/255, 110.0/255, 0.0/255), (0.0/255, 187.0/255, 173.0/255), (209.0/255, 99.0/255, 230.0/255), (178.0/255, 69.0/255, 2.0/255), (255.0/255, 146.0/255, 135.0/255), (89.0/255, 84.0/255, 214.0/255), (0.0/255, 198.0/255, 248.0/255), (135.0/255, 133.0/255, 0.0/255), (0.0/255, 167.0/255, 108.0/255), (189.0/255, 189.0/255, 189.0/255)]

for graph in [
              {"file": "delays-date:2h...csv", "name": "2h"},
              {"file": "delays-date:24h...csv", "name": "24h"},
              {"file": "delays-date:1w...csv",  "name": "7d"},
              {"file": "delays-date:30d...csv",  "name": "30d"}]:
              #{"file": "delays-date:365d...csv",  "name": "365d"}]:
    delays = pd.read_csv(graph["file"])

    delays.sort_values(by='ts', inplace=True)
    delays['ts'] = pd.to_datetime(delays['ts'],origin='unix',unit='s',utc=True)
    delays['t_total'] = delays['t_total'] / 60
    delays['t_ipo'] = delays['t_ipo'] / 60
    delays['t_umail'] = delays['t_umail'] / 60
    delays['t_outlook'] = delays['t_outlook'] / 60

    ipo_list = delays['ipo'].tolist()
    color_list = list(map(lambda x: xgfs_normal12[int(x)],ipo_list))

    #delays['ts'] = delays['ts'].dt.tz_convert('US/Mountain')


    # plot
    fig, ax = plt.subplots()

    fig.set_size_inches(8, 5)
    fig.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.1)

    plt.yscale('log')
    #fig, ax = plt.subplots()
    #plt.yticks(list(map(math.log,[1.0/60,1,10,30,60,600,60*24])),["1 second","1 minute","10 minutes","30 minutes","1 hour", "10 hours", "1 day"])
    plt.yticks([1.0/60,10.0/60,1,10,60,600,60*24],
               ["1 second","10 seconds","1 minute","10 minutes","1 hour","10 hours","1 day"])
    #plt.yticks([10.0/60,5,30,120,1200],["10","5","30","20","2"], minor=True)
    #ax.tick_params(axis='y', which='minor', colors='grey')
    #ax.yaxis.set_minor_tick_formatter()
    #plt.xscale('log')
    #ax.yscale('log')

    plt.title("Time From Receipt by UMail to Delivery: Last {}".format(graph["name"]))
    if graph["name"] == "24h":
        plt.xlabel('Time')
        xfmt = matplotlib.dates.DateFormatter('%H:%M',tz=tz)
        ax.xaxis.set_major_formatter(xfmt)
    else:
        plt.xlabel('Date')
    #plt.ylabel('Fraction')
    plt.ylabel('Time (m)')

    ax.scatter(delays['ts'], delays['t_total'],s=2,c=color_list)
    #ax.ecdf(delays['delay'])

    plt.savefig("{}.png".format(graph["name"]), dpi=150)

    f.write("<div class=\"period\">")
    f.write("<img class=\"graph\" src=\"./{}.png\">".format(graph["name"]))

    f.write("<div class=\"quants\">")
    f.write("<b>Total:</b>")
    f.write("<span class=\"quant\"><span class=\"quantlabel\">median</span> <span class=\"quantval\">{:.2f} minutes</span></span> ".format(delays['t_total'].quantile(q=0.5)))
    f.write("<span class=\"quant\"><span class=\"quantlabel\">75th%</span> <span class=\"quantval\">{:.2f} minutes</span></span> ".format(delays['t_total'].quantile(q=0.75)))
    f.write("<span class=\"quant\"><span class=\"quantlabel\">90th%</span> <span class=\"quantval\">{:.2f} minutes</span></span> ".format(delays['t_total'].quantile(q=0.90)))
    f.write("<span class=\"quant\"><span class=\"quantlabel\">95th%</span> <span class=\"quantval\">{:.2f} minutes</span></span> ".format(delays['t_total'].quantile(q=0.95)))
    f.write("<span class=\"quant\"><span class=\"quantlabel\">99th%</span> <span class=\"quantval\">{:.2f} minutes</span></span> ".format(delays['t_total'].quantile(q=0.99)))
    f.write("</div>\n")

    f.write("<div class=\"quants\">")
    f.write("<b>Ironport:</b>")
    f.write("<span class=\"quant\"><span class=\"quantlabel\">median</span> <span class=\"quantval\">{:.2f} minutes</span></span> ".format(delays['t_ipo'].quantile(q=0.5)))
    f.write("<span class=\"quant\"><span class=\"quantlabel\">75th%</span> <span class=\"quantval\">{:.2f} minutes</span></span> ".format(delays['t_ipo'].quantile(q=0.75)))
    f.write("<span class=\"quant\"><span class=\"quantlabel\">90th%</span> <span class=\"quantval\">{:.2f} minutes</span></span> ".format(delays['t_ipo'].quantile(q=0.90)))
    f.write("<span class=\"quant\"><span class=\"quantlabel\">95th%</span> <span class=\"quantval\">{:.2f} minutes</span></span> ".format(delays['t_ipo'].quantile(q=0.95)))
    f.write("<span class=\"quant\"><span class=\"quantlabel\">99th%</span> <span class=\"quantval\">{:.2f} minutes</span></span> ".format(delays['t_ipo'].quantile(q=0.99)))
    f.write("</div>\n")

    f.write("<div class=\"quants\">")
    f.write("<b>On-prem Exchange:</b>")
    f.write("<span class=\"quant\"><span class=\"quantlabel\">median</span> <span class=\"quantval\">{:.2f} minutes</span></span> ".format(delays['t_umail'].quantile(q=0.5)))
    f.write("<span class=\"quant\"><span class=\"quantlabel\">75th%</span> <span class=\"quantval\">{:.2f} minutes</span></span> ".format(delays['t_umail'].quantile(q=0.75)))
    f.write("<span class=\"quant\"><span class=\"quantlabel\">90th%</span> <span class=\"quantval\">{:.2f} minutes</span></span> ".format(delays['t_umail'].quantile(q=0.90)))
    f.write("<span class=\"quant\"><span class=\"quantlabel\">95th%</span> <span class=\"quantval\">{:.2f} minutes</span></span> ".format(delays['t_umail'].quantile(q=0.95)))
    f.write("<span class=\"quant\"><span class=\"quantlabel\">99th%</span> <span class=\"quantval\">{:.2f} minutes</span></span> ".format(delays['t_umail'].quantile(q=0.99)))
    f.write("</div>\n")

    f.write("<div class=\"quants\">")
    f.write("<b>Outlook:</b>")
    f.write("<span class=\"quant\"><span class=\"quantlabel\">median</span> <span class=\"quantval\">{:.2f} minutes</span></span> ".format(delays['t_outlook'].quantile(q=0.5)))
    f.write("<span class=\"quant\"><span class=\"quantlabel\">75th%</span> <span class=\"quantval\">{:.2f} minutes</span></span> ".format(delays['t_outlook'].quantile(q=0.75)))
    f.write("<span class=\"quant\"><span class=\"quantlabel\">90th%</span> <span class=\"quantval\">{:.2f} minutes</span></span> ".format(delays['t_outlook'].quantile(q=0.90)))
    f.write("<span class=\"quant\"><span class=\"quantlabel\">95th%</span> <span class=\"quantval\">{:.2f} minutes</span></span> ".format(delays['t_outlook'].quantile(q=0.95)))
    f.write("<span class=\"quant\"><span class=\"quantlabel\">99th%</span> <span class=\"quantval\">{:.2f} minutes</span></span> ".format(delays['t_outlook'].quantile(q=0.99)))
    f.write("</div>\n")
#    print("75th%:  {}".format(delays['delay'].quantile(q=0.75)))
#    print("90th%:  {}".format(delays['delay'].quantile(q=0.90)))
#    print("95th%:  {}".format(delays['delay'].quantile(q=0.95)))
#    print("99th%:  {}".format(delays['delay'].quantile(q=0.99)))
    f.write("</div>\n")


f.write("<span class=\"lastupdate\">last updated {}</span>".format(time.strftime("%H:%M %Z %m/%d/%y",time. localtime())))
f.write("</body></html>\n")
