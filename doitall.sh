#!/bin/sh

cd /home/ricci/umail

/usr/local/bin/mbsync -c /home/ricci/umail/mbsyncrc umail-all && /usr/local/bin/notmuch --config /home/ricci/umail/notmuch-config new && /usr/local/bin/python3 getdelay.py && python3 makepage.py && sh copypage.sh
