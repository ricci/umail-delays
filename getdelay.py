#!/usr/bin/env python3

import email
import re
import datetime
import subprocess
import pandas as pd
import math

cache = dict()
try: 
    cachedata = pd.read_csv("cache.csv")
    cache = cachedata.set_index('fname').T.to_dict('list')
except:
    pass

print("Loaded {} entries from cache".format(len(cache)))

# Basic stats
cachehit   = 0
cachemiss  = 0
skips      = 0

# Identify which step of the mail process we're on
def is_ipo(svr):
    return "cc.utah.edu" in svr
def is_umail(svr):
    return "umail.utah.edu" in svr
def is_outlook(svr):
    return "outlook.com" in svr

# Extract the number of the ironport
def ip_nr(svr):
    match = re.search(r"ipo(\d+).*\.cc",svr)
    if match:
        return match[1]
    else:
        return none

def match_it(hop):
    return re.search(r"from\s+(\(?[^\s]+)\s.*\s+by\s+([^\s]+)\s.*;\s+(\w\w\w,)?\s*(\d+)\s+(\w+)\s+(\d+)\s+(\d+:\d+:\d+)\s+([-+]\d+)", hop, flags= re.MULTILINE | re.DOTALL)

for period in ['date:2h..', 'date:24h..','date:1w..','date:30d..']:
    result = subprocess.run(['notmuch','--config','/home/ricci/umail/notmuch-config','search','--format=text','--output=files','--',period], stdout=subprocess.PIPE)
    lines = result.stdout.decode('utf-8')
    lines = lines.splitlines()
    with open("delays-{}.csv".format(period),'w', encoding="utf-8") as o:
        # CSV header
        o.write("msgid,ts,ipo,t_total,t_ipo,t_umail,t_outlook\n")
        for line in lines:
            # Start empty
            if line in cache:
                cachehit = cachehit +1
                o.write("{},{},{},{},{},{},{}\n".format(*cache[line]))
            else:
                cachemiss = cachemiss +1
                first_ipo, first_umail, first_outlook, last, ipo = None, None, None, None, None
                with open(line,'r') as f:
                    try:
                        parsed_message = email.message_from_file(f)
                    except:
                        skips = skips +1
                        continue

                    received = parsed_message.get_all('Received')
                    if received:
                        for hop in received:
                            match = match_it(hop)
                            #  Sat, 27 Jan 2024 08:55:36 -0700
                            if match:
                                prev, cur, day, date, month, year, time, tz = match.groups()
                                ts = datetime.datetime.strptime(f"{date} {month} {year} {time} {tz}", "%d %b %Y %H:%M:%S %z")
                                if last is None:
                                    last = ts
                                if is_ipo(cur) and not is_ipo(prev):
                                    first_ipo = ts
                                    ipo = ip_nr(cur)
                                if is_umail(cur) and not is_umail(prev):
                                    first_umail = ts
                                if is_outlook(cur) and not is_outlook(prev):
                                    first_outlook = ts
                if first_ipo and last:
                    cache[line] = [parsed_message.get("Message-ID"),
                                   first_ipo.timestamp(),
                                   int(ipo),
                                   (last - first_ipo).total_seconds(),
                                   (first_umail - first_ipo).total_seconds(),
                                   (first_outlook - first_umail).total_seconds(),
                                   (last - first_outlook).total_seconds()]
                    o.write("{},{},{},{},{},{},{}\n".format(*cache[line]))
                else:
                    skips = skips +1

print("Finished with {} hits, {} misses, {} skips".format(cachehit,cachemiss,skips))

pd.DataFrame.from_dict(cache,orient='index').to_csv("cache.csv",index_label="fname")
